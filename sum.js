var sum = 0;

function sumTo(n){
	
		for ( var i = 1; i <= n; i++ ){
		sum += i;
		}
		
	console.log('Простая сумма чисел от 1 до '+n+' = '+sum);
}	

sumTo(7);

function sumToRecursion(sum,j,n){
	if ( j <= n ){
		sum += j;
		return sumToRecursion(sum, ++j,n);
	}
	else{
		console.log('Рекурсивная сумма чисел от 1 до '+n+' = '+sum);
		return sum;
	}
}

sumToRecursion(0,1,7);