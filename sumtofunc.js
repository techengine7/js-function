/*Напишите функцию sumTo(n), которая для данного n вычисляет сумму чисел от 1 до n,
например: sumTo(3) = 1 + 2 + 3, sumTo(2) = 1 + 2 и т.п.

Решите задачу двумя способами (рекурсиеи? и с помощью циклов). Какои? способ, по-вашему, лучше?*/

function sumTo(n){
	if ( n > 1 ) {
	var sum = 0;
	
		for ( var i = 1; i <= n; i++ ){
		sum += i;
		}
		console.log('Простая сумма чисел от 1 до '+n+' = '+sum);
		
		var i = 0, sum = 0;
		sumToRecursion(n);
		
		function sumToRecursion(n){
			if ( i <= n ){
				sum += i;
				i++;
				sumToRecursion(n);
			}
			else{
				console.log('Рекурсивная сумма чисел от 1 до '+n+' = '+sum);
				return sum;
			}
		}
		
	}
	else 
		console.log('укажите большее значение для посчета');
}

sumTo(7);