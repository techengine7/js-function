	
	var products = ['apples', 		20, //количество на складе продуктов
					'strawberry', 	20, 
					'apricot', 		20, 
					'flour', 		20, 
					'milk', 		20, 
					'eggs', 		50 
					];
	
	var recipeApplePie = [ //рецепт яблочного пирога
					'apples', 3, 
					'flour', 2, 
					'milk', 1, 
					'eggs', 3
					];
					
	var recipeStrawberryPie = [ //рецепт клубничного пирога
					'strawberry', 5, 
					'flour', 1, 
					'milk', 2, 
					'eggs', 4
					];
					
	var recipeApricotPie = [ //рецепт абрикосового пирога
					'apricot', 2, 
					'flour', 3, 
					'milk', 2, 
					'eggs', 2
					];
	
	var applePie = 0; // количество яблочных пирогов, которое испекли.
	var strawberryPie = 0; // количество клубничных пирогов, которое испекли.
	var apricotPie = 0; // количество абрикосовых пирогов, которое испекли.

	function purchase(ingredient, ended, quantity, unit){ //Закупка ингредиентов
		quantity = quantity || 10; //присвоение значения по умолчанию
		products[ingredient+1] += quantity;
		console.log('          '+ended+'! Купили еще '+quantity+unit);
		return products;
	}
	
	function bakePie(name, numPie, recipe, products) { // Производство пирога
		for ( i=0; i<recipe.length; i+=2  ){
			var ingredient = products.indexOf(recipe[i]); //индекс ингредиента на складе продуктов
			
				if (products[ingredient+1] - recipe[i+1]  <  0) {//проверка необходимого наличия ингредиентов
					switch(products[ingredient]) {
						case 'apples':
							purchase(ingredient, 'Кончились яблоки!', 10, ' кг');
						break;
						case 'strawberry':
							purchase(ingredient, 'Кончилась клубника!', 10, ' кг');
						break;
						case 'apricot':
							purchase(ingredient, 'Кончились абрикосы!', 10, ' кг');
						break;
						case 'flour':
							purchase(ingredient, 'Кончилась мука!', 10, ' кг');
						break;
						case 'milk':
							purchase(ingredient, 'Кончилось молоко!', 10, ' литров');
						break;
						case 'eggs':
							purchase(ingredient, 'Кончились яйца!', 10, ' штук');
						break;
					}	
				}
			
			products[ingredient+1] = products[ingredient+1] - recipe[i+1];
		}
	console.log(name+' №'+numPie+' готов!');
	}
	
	for (pie = 0; pie < 10; pie++) {
		bakePie('Яблочный пирог', ++applePie, recipeApplePie, products);
		bakePie('Клубничный пирог', ++strawberryPie, recipeStrawberryPie, products);
		bakePie('Абрикосовый пирог', ++apricotPie, recipeApricotPie, products);
	}
	
